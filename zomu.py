from flask import Flask, request, jsonify, send_from_directory
import base64, requests

url_anonymizer = Flask(__name__)

@url_anonymizer.route("/<path:url>")
def default(url):
    
    # decoding base64 to url
    padding = len(url) % 4
    if padding:
        url += '=' * (4 - padding)
    url = base64.b64decode(url).decode('utf-8')

    # Tiktok
    if "tiktok" in url:
        headers = {'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 17_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.1 Mobile/15E148 Safari/604.1'}
        response = requests.get(url, headers=headers, allow_redirects=True)
        return response.url.split('?', 1)[0]

    # Amazon
    # more challenging than the others because we only need a "middle" part of the URL (dp/<code>)
    #
    # could break when shortening foreign amazon URLs due to localization of URLs because of stuff like ../en-US/..
    elif "amazon" in url:
        url = url.replace("https://", "").replace("http://", "").replace("www.", "")
        first_slash_index = url.find("/")
        second_slash_index = url.find("/", first_slash_index + 1)
        url = url[:first_slash_index + 1] + url[second_slash_index + 1:]
        last_slash_index = url.rfind("dp/") + len("dp/") + 10
        return url[:last_slash_index]
    
    # Reddit
    elif "redd" in url:
        return url[:url.rfind("/")]
     
    # Youtube Instagram Ebay Spotify
    elif "outu" in url or "instagram" in url or "ebay" in url or "spotify" in url:
        return url.split('?', 1)[0]

    else:
        return 'ERROR: invoke the API with base64 encoded URL'

@url_anonymizer.route("/favicon.ico")
def favicon():
    return "", 204

if __name__ == "__main__":
    url_anonymizer.run()
