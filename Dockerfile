FROM python:3-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000

CMD [ "python3", "./zomu.py" ]
CMD ["gunicorn", "-w", "1", "-b", "0.0.0.0:5000", "zomu:url_anonymizer"]

