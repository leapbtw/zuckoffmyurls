# Zuck Off My URLs!

The purpose of this software is make URLs safe to share without worring about online privacy.
Usually social media platforms add tracking, Sharing IDs, telemetry and such to URLs when users click the share button, this software aims to remove those things and provide a clean link to the same post/content.

Supports Tiktok, Instagram, Youtube, Reddit, Amazon, Ebay and Spotify! More platform coming soon question mark?

# Deploying
- clone the repo ```git clone https://gitlab.com/leapbtw/zuckoffmyurls.git```
- install [Docker](docker.com)
- build with the [Dockerfile](../Dockerfile) with ```docker build -t leapbtw/zomu:1.0 .```
- run with ```docker run -d -p 5000:5000 --name zomu leapbtw/zomu:1.0```exposing port 5000 (or modify the Dockerfile if you want to use a different one)

# How to use
- find a URL you want to clean
- base64 encode the URL
- call the API like this (http://127.0.0.1:5000/base64_encoded_URL)
the software will return a clean url

# Known issues
- obviously doesn't support https by itself, you might want to use that since you're here for privacy
- if the input URL was base64 encoded with extra space/endl characters, it might _zuck_ up and fail
